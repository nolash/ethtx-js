import { strip0x, add0x } from './hex';
import { Keccak } from 'sha3';
const RLP = require('rlp');

function isAddress(a:Uint8Array):boolean {
	return a !== undefined && a.length == 20;
}

function toValue(n:number):bigint {
	return BigInt(n);
}

function stringToValue(s:string):bigint {
	return BigInt(s);
}

function hexToValue(hx:string):bigint {
	return BigInt(add0x(hx));
}

class Tx {

	nonce: number
	gasPrice: number
	gasLimit: number
	to: Uint8Array
	value: bigint
	data: Uint8Array
	v: number
	r: Uint8Array
	s: Uint8Array
	chainId: number
	_signatureSet: boolean
	_workBuffer: ArrayBuffer
	_outBuffer: DataView
	_outBufferCursor: number

	constructor(chainId:number) {
		this.chainId = chainId;

		this.nonce = 0;
		this.gasPrice = 0;
		this.gasLimit = 0;
		this.to = new Uint8Array(32);
		this.data = new Uint8Array(0);
		this.value = BigInt(0);

		this._workBuffer = new ArrayBuffer(32);
		this._outBuffer = new DataView(new ArrayBuffer(1024*1024));
		this._outBufferCursor = 0;

		this.clearSignature();
	}

	private serializeNumber(n:bigint): Uint8Array {
		let view = new DataView(this._workBuffer);
		view.setBigUint64(0, BigInt(0));
		view.setBigUint64(0, n);
		let zeroOffset = 0;
		for (zeroOffset = 0; zeroOffset < 8; zeroOffset++) {
			if (view.getInt8(zeroOffset) > 0) {
				break;
			}
		}
		return new Uint8Array(this._workBuffer).slice(zeroOffset, 8);
	}

	private write(data:Uint8Array) {
		data.forEach((v) => {
			this._outBuffer.setInt8(this._outBufferCursor, v);
			this._outBufferCursor++;
		});
	}


	public serializeBytes(): Uint8Array {
		if (!isAddress(this.to)) {
			throw new Error('invalid address');
		}
		
		const nonce = this.serializeNumber(BigInt(this.nonce));
		this.write(nonce);

		const gasPrice = this.serializeNumber(BigInt(this.gasPrice));
		this.write(gasPrice);

		const gasLimit = this.serializeNumber(BigInt(this.gasLimit));
		this.write(gasLimit);

		this.write(this.to);

		const value = this.serializeNumber(this.value);
		this.write(value);

		this.write(this.data);

		const v = this.serializeNumber(BigInt(this.v));
		this.write(v);
		
		this.write(this.r);
		this.write(this.s);

		return new Uint8Array(this._outBuffer.buffer).slice(0, this._outBufferCursor);
	}

	public canonicalOrder():Uint8Array[] {
		return [
			this.serializeNumber(BigInt(this.nonce)),
			this.serializeNumber(BigInt(this.gasPrice)),
			this.serializeNumber(BigInt(this.gasLimit)),
			this.to,
			this.serializeNumber(this.value),
			this.data,
			this.serializeNumber(BigInt(this.v)),
			this.r,
			this.s,

		];
	}

	public serializeRLP():Uint8Array {
		return RLP.encode(this.canonicalOrder());
	}

	public message():Uint8Array {
		// TODO: Can we do without Buffer, pleeease?
		const h = new Keccak(256);
		const b = new Buffer(this.serializeRLP());
		h.update(b);
		return h.digest();
	}

	public setSignature(r:Uint8Array, s:Uint8Array, v:number) {
		if (this._signatureSet) {
			throw new Error('Signature already set');
		}
		if (r.length != 32 || s.length != 32) {
			throw new Error('Invalid signature length');
		}
		if (v < 0 || v > 3) {
			throw new Error('Invalid recid');
		}	
		this.r = r;
		this.s = s;
		this.v = (this.chainId * 2) + 35 + v;
		this._signatureSet = true;
	}

	public clearSignature() {
		this.r = new Uint8Array(0);
		this.s = new Uint8Array(0);
		this.v = this.chainId;
		this._signatureSet = false;
	}
}

export {
	Tx,
	stringToValue,
	hexToValue,
	toValue,
}
