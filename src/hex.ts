// improve
function validHex(hexString:string):string {
	return hexString;
}

function even(hexString:string):string {
	if (hexString.length % 2 != 0) {
		hexString = '0' + hexString;
	}
	return hexString;
}

function strip0x(hexString:string):string {
	if (hexString.length < 2) {
		throw new Error('invalid hex');
	} else if (hexString.substring(0, 2) == '0x') {
		hexString = hexString.substring(2);
	}
	return validHex(even(hexString));
}

function add0x(hexString:string):string {
	if (hexString.length < 2) {
		throw new Error('invalid hex');
	} else if (hexString.substring(0, 2) != '0x') {
		hexString = '0x' + hexString;
	}
	return validHex(even(hexString));
}

function fromHex(hexString:string):Uint8Array {
	return new Uint8Array(hexString.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));
}

function toHex(bytes:Uint8Array):string {
	return bytes.reduce((str, byte) => str + byte.toString(16).padStart(2, '0'), '');
}

export {
	fromHex,
	toHex,
	strip0x,
	add0x,
}
