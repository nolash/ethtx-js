import { Tx, toValue } from '../src/tx';
import { fromHex, toHex } from '../src/hex';
import assert = require('assert');
import * as secp256k1 from 'secp256k1';
const RLP = require('rlp');

const privateKey = fromHex('5087503f0a9cc35b38665955eb830c63f778453dd11b8fa5bd04bc41fd2cc6d6');
const address = fromHex('eb3907ecad74a0013c259d5874ae7f22dcbcc95c');

describe('tx', async () => {
	it('create', () => {
		let tx = new Tx(8996);

		tx.nonce = 42;
		tx.gasPrice = 1000000000;
		tx.gasLimit = 21000;
		tx.to = address;
		tx.value = toValue(1024);

		const tx_bytes = tx.serializeBytes();
		console.log(toHex(tx_bytes));

		const tx_canonical = tx.canonicalOrder();
		console.log(toHex(RLP.encode(tx_canonical)));

		const tx_msg = tx.message();
		console.log(toHex(tx_msg));

		const sigObj = secp256k1.ecdsaSign(tx_msg, privateKey);
		console.log(sigObj);
		const r = sigObj.signature.slice(0, 32);
		const s = sigObj.signature.slice(32);
		const v = sigObj.recid;

		tx.setSignature(r, s, v);
		tx.clearSignature();
		tx.setSignature(r, s, v);

		const tx_wire = tx.serializeRLP();
		console.log(toHex(tx_wire));
	});
});
